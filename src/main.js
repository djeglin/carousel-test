import Vue from 'vue'
import VueAxios from 'vue-axios'
import axios from 'axios'

import App from './App'

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
